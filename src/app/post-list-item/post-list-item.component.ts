import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {

  @Input() PostItem: Post;
  
  constructor() { }

  ngOnInit() {
  }

}

export class Post {
  title: string;
  content: string;
  loveIts: number;
  created_at: Date;

  constructor( title, content ) {
    this.title = title;
    this.content = content;
    this.loveIts = 0;
    this.created_at = new Date();
  }

  like() {
    this.loveIts++;
  }

  unlike() {
    this.loveIts--;
  }

  getColor(){
    if(this.loveIts>0)
      return 'green';
    else if(this.loveIts<0)
      return 'red';
  }
}
