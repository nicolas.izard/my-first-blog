import { Component } from '@angular/core';
import { Post } from './post-list-item/post-list-item.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  posts = [
    new Post("Mon premier post", "Les pluies se sont installées dans le Sud-Est de la France, à commencer par le Languedoc : orages et pluies sont prévus toute la journée de ce mercredi 11 avril. "),
    new Post("Mon deuxième post", "Demain un épisode de pluies soutenues est prévu sur les régions méditerranéennes avec 40 à 80 mm de pluie prévus en plaine, et jusqu'à 100 à 120 mm sur le relief des Cévennes."),
    new Post("Mon dernier post", " Soleil attendu rapidement sur l'ensemble du territoire! ;-)"),
  ];

}
